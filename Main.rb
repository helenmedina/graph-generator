require_relative 'XmlParser.rb'
require 'erb'


puts 'File name xml to parse: '
STDOUT.flush  
fileName = gets.chomp

if not fileName.match(/.xml\z/)
 fileName = fileName + '.xml'
end


begin

	doc = REXML::Document.new( File.open( "#{fileName}" ) )
	#parse the input file
	parser = XmlParser.new
	parser.parse(doc)
	
	#get the arrays with the table, columns information
	tables = parser.getTables
	#get the arrays with theforeign keys information
	foreignKeyIDs = parser.GetForeignkeyIDs

	#create the ERB object to bind the arrays 
	erb = ERB.new( File.open( "XmlTemplate.graphml" ).read )
	newGraphmlFile = erb.result( binding )
	class_name = "RelationalDatabase"
	print "Creating #{class_name}.graphml\n"

	#create the output file with the arrays and template information
	File.open( "#{class_name}.graphml", "w" ).write( newGraphmlFile )

rescue REXML::ParseException => e
	puts "Failed when parsing document: #{e.message}"
rescue RuntimeError => e
	puts "Failed: #{e.message}"
rescue 
	puts "Failed: #{$!.message}" 
end


