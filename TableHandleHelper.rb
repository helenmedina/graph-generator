class TableHandleHelper

	SForeignKeyIDs = Struct.new(:id, :source, :target, :label)
	
	def initialize
		@arrayForeignKeyIDs = []
	end
	
	def getIDTable(counter)
		id = "n" + counter.to_s
		return id
	end
	
	def SetUpForeignKey(source, counter, target, label)
		idTable = source.getID
		@IDForeignKey = idTable + "_e" + counter.to_s
		@foreignKeyIDs = SForeignKeyIDs.new(@IDForeignKey, idTable, target, label) 
		@arrayForeignKeyIDs.push(@foreignKeyIDs)
	
	end
	
	def HandleForeignKeyIDs(tables)
		@arrayForeignKeyIDs.each { |fk| 
			bFound = false
			nameTarget = fk.target
			tables.each{ |table|
				if (nameTarget.downcase == table.getName.downcase)
					fk[:target] = table.getID
					bFound = true
				end
			}
			if(!bFound)
				raise "The foreign key \"#{nameTarget}\" is not a valid table"
			end
		}
	end
	
	def GetForeignKeyIDs
		return @arrayForeignKeyIDs
	end
	
	def darken_color(hexColor, amount=0.85)
		hexColor = hexColor.gsub('#','')
		rgb = hexColor.scan(/../).map {|color| color.hex}
		rgb[0] = (rgb[0].to_i * amount).round
		rgb[1] = (rgb[1].to_i * amount).round
		rgb[2] = (rgb[2].to_i * amount).round
		color = "#%02x%02x%02x" % rgb
		return color
		
	end
	
	def getColorTable(color)
		case color.downcase
			when "blue"
				color = '#E8EEF7'
			when "purple"
				color = '#D9ADA7'
			when "green"
				color = '#ccff33'
			when "red"
				color = '#FF0000'
			when "yellow"
				color = '#ffff99'
			when "brown"
				color = '#ffcc66'
			when "gray"
				color = '#cccccc'
			when "pink"
				color = '#ffcccc'
			when "orange"
				color = '#ff9900'
			end
		return color
	end
	
	def setDefaultValueIfEmpty(fields)
	
		fields.each { |key, value| 
		if(value == nil || value.empty?)
			case key
				when :height
					value.replace("90")
				when :width
					value.replace("80")
				when :color
					value.replace("blue")
				when :primaryKey
					value.replace("false")
				when :secondaryKey
					value.replace("false")
				when :label
					value.replace("")
				end
			end
		 }
		 return fields
	
	end

end
