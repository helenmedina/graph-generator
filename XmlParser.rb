require 'rexml/document'
require "rexml/parseexception"
require_relative 'Table.rb'
require_relative 'TableHandleHelper.rb'

class XmlParser

	TableHelper = TableHandleHelper.new
	
	def initialize
		@arrayTables = []
	end
	
	def getTables
		return @arrayTables
	end
	
	def GetForeignkeyIDs
		return TableHelper.GetForeignKeyIDs
	end
	
	def raiseErrorIfEmpty(name, field, tableName = nil)
		if (name == nil || name.empty?)
			if (tableName != nil)
				raise "The #{field} name of the table #{tableName} is null or empty"
			else
				raise "The #{field} name is null or empty"
			end
		end
	end
	
	def SaveColorsTable(color, table)
		#if color is not a valid hexadecimal format the system raise an error
		colorTableHex = TableHelper.getColorTable(color)
		unless colorTableHex =~ /^#[0-9A-Fa-f]{6}$/
			raise "Color format: #{colorTableHex} not valid."
		end
		
		# get a darker color header
		colorLabelHex = TableHelper.darken_color(colorTableHex)

		#store the color table and the color header in the array Table
		table.insertColors({colorTable: colorTableHex, colorLabel: colorLabelHex})
		
	end

	def saveColumsToTable(fields, table)
		# get the attributes <column>
		fields.each_element( "columns/column" ) { |column|
			bPrimaryKey = column.attributes[ "primary-key" ]
			bSecondaryKey = column.attributes[ "foreign-key" ]
			
			#the attribute name is mandatory, if it is not found the system raise an error
			raiseErrorIfEmpty(column.text, "column", table.getName)
			name = column.text.strip
			
			#create a hash that store the column attributes
			hash = Hash.new
			hash.store(:primaryKey, "#{bPrimaryKey}")
			hash.store(:secondaryKey, "#{bSecondaryKey}")
			hash = TableHelper.setDefaultValueIfEmpty(hash)
			bPrimaryKey = "#{hash[:primaryKey]}"
			bSecondaryKey = "#{hash[:secondaryKey]}"

			#store the column table in the array Table
			table.insertColum({name: name, isPrimaryKey: bPrimaryKey, isForeignKey: bSecondaryKey})
		}
	end
	
	def saveForeignKeysToTable(fields, table)
	
		counter = 0
		# get the attributes <foreign-key>
		fields.each_element( "foreign-keys/foreign-key" ) { |key|
			label = key.attributes[ "label" ]
			
			#the attribute name is mandatory, if it is not found the system raise an error
			raiseErrorIfEmpty(key.text, "foreign-Key", table.getName)
			fkName = key.text.strip
			
			#create a hash that store the foreign-key attributes
			hash = Hash.new
			hash.store(:label, "#{label}")
			hash = TableHelper.setDefaultValueIfEmpty(hash)
			label = "#{hash[:label]}"
			#get a unique id foreign-key
			TableHelper.SetUpForeignKey(table, counter, fkName, label)
			counter = counter + 1
		}
	end
	
	def parse(doc)
	
		counter = 0
		doc.root.each_element( "table" ) { |fields| 
			name = fields.attributes[ "name" ]
			height = fields.attributes[ "height" ]
			witdh = fields.attributes[ "width" ]
			color = fields.attributes[ "color" ]
			
			#the attribute name is mandatory, if it is not found the system raise an error
			raiseErrorIfEmpty(name, "table")
			#remove white spaces
			name.gsub!(/\s+/, '')
			
			#Get default values
			hash = Hash.new
			hash.store(:height, "#{height}")
			hash.store(:width, "#{witdh}")
			hash.store(:color, "#{color}")
			
			hash = TableHelper.setDefaultValueIfEmpty(hash)
			
			height = "#{hash[:height]}"
			witdh = "#{hash[:width]}"
			color = "#{hash[:color]}"
			
			#get a unique id table
			id = TableHelper.getIDTable(counter)
			#create a table object with the file information
			table = Table.new(name, height, witdh, id)
			SaveColorsTable(color, table)
			saveColumsToTable(fields, table)
			saveForeignKeysToTable(fields, table)
			
			@arrayTables.push(table)
			counter += 1
		}
		#for every foreign-key, look for the parent table
		TableHelper.HandleForeignKeyIDs(@arrayTables)

	end
	
	
end

