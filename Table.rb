class Table

	SColumns = Struct.new(:name, :isPrimaryKey, :isForeignKey)
	SColor = Struct.new(:colorTable, :colorLabel)

	attr_accessor :name, :height, :width, :color

	def initialize(name, height, width, id)
		@name = name
		@height = height
		@width = width
		@color = []
		@id = id
		@arrayColumn = []
	end

	def insertColum(fields)

		@columns = SColumns.new(fields[:name], fields[:isPrimaryKey], fields[:isForeignKey]) 
		@arrayColumn.push(@columns)

	end
	
	def insertColors(color)
	
		@color = SColor.new(color[:colorTable], color[:colorLabel]) 
		
	end

	def getColumns
	 return @arrayColumn
	end

	def getName
		return @name
	end

	def getHeight
		return @height
	end

	def getWidth
		return @width
	end
	
	def getColor
		return @color
	end
	
	def getID
		return @id
	end
end



